<?php

namespace App\Models;

use App\Enums\FeedbackSourceEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feedback extends Model
{
    protected $fillable = [
        'user_id',
        'content',
        'rating',
        'address',
        'apartments',
        'source',
    ];

    protected $casts = [
        'source' => FeedbackSourceEnum::class,
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
