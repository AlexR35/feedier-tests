<?php

namespace App\Actions;

use App\Models\Feedback;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class FeedbackExportJson
{
    private Carbon $date_from;
    private Carbon $date_to;

    public function __construct(Carbon $date_from, Carbon $date_to)
    {
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }

    private function getFeedbacks(): Collection
    {
        return Feedback::query()
            ->whereBetween('created_at', [$this->date_from, $this->date_to])
            ->get();
    }

    public function asJson()
    {
        return json_encode($this->getFeedbacks()->map(fn (Feedback $feedback) => [
            ...$feedback->only('content', 'rating', 'address', 'apartments', 'source', 'created_at'),
            'user' => $feedback->user?->name,
        ]));
    }
}
