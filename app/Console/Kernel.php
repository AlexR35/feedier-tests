<?php

namespace App\Console;

use App\Console\Commands\FeedbackExportAdmin;
use App\Console\Commands\FeedbackImportCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command(FeedbackImportCommand::class)->hourly();
        $schedule->command(FeedbackExportAdmin::class)->fridays()->at('15:00');
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
