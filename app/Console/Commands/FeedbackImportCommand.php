<?php

namespace App\Console\Commands;

use App\Imports\FeedbackImport;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class FeedbackImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:feedback:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import feedback from a CSV source';

    /**
     * Sources in CSV format to import.
     *
     * @var array
     */
    protected const CSV_SOURCES = [
        'https://feedier-prod-europe.s3.eu-west-1.amazonaws.com/special/Reviews+Import.csv',
    ];

    /**
     * Sources in CSV format to import.
     *
     * @var array
     */
    protected const DISK = 'local';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        foreach (self::CSV_SOURCES as $source) {
            $this->importFromSource($source);
        }
    }

    private function importFromSource(string $url)
    {
        $this->info("Importing feedbacks from source: $url");

        $contents = file_get_contents($url);
        $fileName = uniqid('imported-feedbacks-') . '.csv';

        $this->disk()->put($fileName, $contents);
        Excel::import(new FeedbackImport, Storage::disk(self::DISK)->path($fileName));
        $this->disk()->delete($fileName);
    }

    private function disk(): Filesystem
    {
        return Storage::disk(self::DISK);
    }
}
