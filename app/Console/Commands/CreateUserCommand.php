<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user with basic information.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $name = $this->ask("Username");
        $email = $this->ask("Email address");

        $validator = Validator::make([
            'name' => $name,
            'email' => $email
        ], [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
        ]);

        if ($validator->fails()) {
            $this->info("An error occurred during the validation of the filled information:");

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return;
        }

        $password = Str::random(15);
        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        $this->info("User $user->name ($user->email) has been created with password: $password");
    }
}


