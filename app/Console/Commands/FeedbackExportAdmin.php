<?php

namespace App\Console\Commands;

use App\Mail\ExportFeedbackMail;
use App\Models\Feedback;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class FeedbackExportAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:feedback:export-admins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export recent feedbacks to administrators by mail.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $users = User::query()
            ->admin()
            ->get();

        Mail::bcc($users)->send(new ExportFeedbackMail(today()->subWeek(), now()));

        $this->info("Export has been sent to " . $users->count() . " administrator(s).");
    }
}
