<?php

namespace App\Enums;

enum FeedbackSourceEnum: string
{
    case FEEDIER = 'Feedier';
    case GOOGLE = 'Google';
    case UNKNOWN = 'N/A';
}
