<?php

namespace App\Http\Controllers;

use App\Enums\FeedbackSourceEnum;
use App\Http\Requests\FeedbackStoreRequest;
use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia()->render('Feedback/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(FeedbackStoreRequest $request)
    {
        $request->user()->feedbacks()->create([
            ...$request->only('content', 'rating'),
            'source' => FeedbackSourceEnum::FEEDIER,
        ]);

        return redirect()->route('index')->with('success', 'Feedback has been created.');
    }
}
