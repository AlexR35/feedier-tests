<?php

namespace App\Http\Controllers;


use App\Models\Feedback;

class IndexController extends Controller
{
    public function __invoke()
    {
        return inertia()->render('Index')
            ->with('feedbacks', Feedback::query()->latest()->get());
    }
}
