<?php

namespace App\Mail;

use App\Actions\FeedbackExportJson;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Attachment;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class ExportFeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    private Carbon $date_from;
    private Carbon $date_to;

    /**
     * Create a new message instance.
     */
    public function __construct(Carbon $date_from, Carbon $date_to)
    {
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Export Feedback Mail',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'emails.export-feedback',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [
            Attachment::fromData(
                fn () => (new FeedbackExportJson($this->date_from, $this->date_to))->asJson(),
                'export.json'
            )->withMime('application/json'),
        ];
    }
}
