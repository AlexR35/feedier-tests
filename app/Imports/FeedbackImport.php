<?php

namespace App\Imports;

use App\Enums\FeedbackSourceEnum;
use App\Models\Feedback;
use Illuminate\Support\Collection;
use JetBrains\PhpStorm\NoReturn;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FeedbackImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return Feedback
     */
    public function model(array $row): Feedback
    {
        return new Feedback([
            'content' => $row['reviews_content'],
            'rating' => $row['rating'],
            'address' => $row['address'],
            'apartments' => $row['appartments'],
            'source' => FeedbackSourceEnum::tryFrom($row['source'])?->value ?: FeedbackSourceEnum::UNKNOWN,
            'created_at' => $row['start_date'],
            'updated_at' => $row['start_date'],
        ]);
    }
}
