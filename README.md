# FEEDIER TESTS

#### **The goal of this test is to evaluate backend skills on Laravel and validate youʼre a good fit for the Feedier project :-)**  

**What we evaluate:** 
* Code quality
* Usage of Laravel
* Programming logics
* Respect of the instructions
* Performance of the test

**What we donʼt evaluate:**
* Versions/IDE/Repo you use

### Test1
Create a secure and efficient Laravel application that imports feedback data (like reviews) and saves them in your application.
The app must support 2 import types:

* Calling a CSV file on an hourly basis through a CRON job.
* A frontend Vue.js app with a simple text field hosted on Laravel side, when submitted, it must
create a feedback entry in Feedier. Other important requirements:


* An export of the imported feedback is sent to the user with role admin by email every Friday at 3 pm (as a JSON file attached to the email).
* The user can login to the app to import the feedback.
* Sentry is connected to your Laravel app so you receive notifications on Exceptions.

Notes:
1. CSV file to read on hourly basis:
   https://feedier-prod-europe.s3.eu-west-1.amazonaws.com/special/Reviews+Import.csv

### Test2
You receive this error from the product team:
<img src="https://gitlab.com/AlexR35/feedier-tests/-/raw/master/git-medias/Capture%20d%E2%80%99%C3%A9cran%202023-03-02%20%C3%A0%2014.47.38.png" />

Based on your understanding of Laravel Queues,
1. Describe in your own words what this error means and the assumptions you can make.
2. What are your 3 actions or workflows you would suggest to solve the issue with your team?
   Good luck!

<span style="color: green;">**ANSWER:** We can see that the job failed for the last time at 3:57, it's 57 minutes after the first execution (default time for export is friday at 03pm). This can be, for example, the mail server that is down or the code that is not optimized (using `->chunk()` to get all recent feedbacks can be a solution instead of using `->all()`, for example). Without any code, it's difficult to say what's wrong.</span>

<span style="color: green;">**SUGGESTIONS:** </span>  
<span style="color: green;">1. Inspect if the mail server is not down? It can be an issue with the web server if the job is sending a mail.</span>  
<span style="color: green;">2. Inspect the context of the job in the Sentry report, it can be a specific user or specific feedback that had a problem and block the sent of the export.</span>  
<span style="color: green;">3. Add logs to inspect at the next failure and increment the $retry in the job temporarly.</span>  
<span style="color: green;">4. Try to debug in local using the same context and see what's happen.</span>  

## Project illustrations
<img src="https://gitlab.com/AlexR35/feedier-tests/-/raw/master/git-medias/Capture%20d%E2%80%99%C3%A9cran%202023-03-02%20%C3%A0%2014.41.54.png" />
<img src="https://gitlab.com/AlexR35/feedier-tests/-/raw/master/git-medias/Capture%20d%E2%80%99%C3%A9cran%202023-03-02%20%C3%A0%2014.41.58.png" />
<img src="https://gitlab.com/AlexR35/feedier-tests/-/raw/master/git-medias/Capture%20d%E2%80%99%C3%A9cran%202023-03-02%20%C3%A0%2014.42.03.png" />
<img src="https://gitlab.com/AlexR35/feedier-tests/-/raw/master/git-medias/Capture%20d%E2%80%99%C3%A9cran%202023-03-02%20%C3%A0%2014.42.10.png" />
