@component('mail::message')
{{ __('In attachment you will find a JSON export of all feedbacks received this last week.') }}
@endcomponent
